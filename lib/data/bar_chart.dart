import 'package:charts_flutter/flutter.dart' as charts;

List<charts.Series<OrdinalSales, String>> barChart(
    int risk5Hip, risk10Hip, risk5Any, risk10Any) {
  final tableSalesDataA = [
    new OrdinalSales('5 years', 100 - risk5Hip),
    new OrdinalSales('10 years', 100 - risk10Any),
  ];

  final mobileSalesDataA = [
    new OrdinalSales('5 years', risk5Hip),
    new OrdinalSales('10 years', risk10Hip),
  ];

  final tableSalesDataB = [
    new OrdinalSales('5 years', 100 - risk5Any),
    new OrdinalSales('10 years', 100 - risk10Any),
  ];

  final mobileSalesDataB = [
    new OrdinalSales('5 years', risk5Any),
    new OrdinalSales('10 years', risk10Any),
  ];

  return [
    new charts.Series<OrdinalSales, String>(
      id: 'Tablet A',
      seriesCategory: 'A',
      domainFn: (OrdinalSales sales, _) => sales.year,
      measureFn: (OrdinalSales sales, _) => sales.sales,
      fillColorFn: (_, __) => charts.MaterialPalette.transparent,
      data: tableSalesDataA,
    ),
    new charts.Series<OrdinalSales, String>(
      id: 'Mobile A',
      seriesCategory: 'A',
      domainFn: (OrdinalSales sales, _) => sales.year,
      measureFn: (OrdinalSales sales, _) => sales.sales,
      fillColorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
      labelAccessorFn: (OrdinalSales sales, _) => '${sales.sales.toString()}',
      data: mobileSalesDataA,
    ),
    new charts.Series<OrdinalSales, String>(
      id: 'Tablet B',
      seriesCategory: 'B',
      domainFn: (OrdinalSales sales, _) => sales.year,
      measureFn: (OrdinalSales sales, _) => sales.sales,
      fillColorFn: (_, __) => charts.MaterialPalette.transparent,
      data: tableSalesDataB,
    ),
    new charts.Series<OrdinalSales, String>(
      id: 'Mobile B',
      seriesCategory: 'B',
      domainFn: (OrdinalSales sales, _) => sales.year,
      measureFn: (OrdinalSales sales, _) => sales.sales,
      fillColorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      labelAccessorFn: (OrdinalSales sales, _) => '${sales.sales.toString()}',
      data: mobileSalesDataB,
    ),
  ];
}

class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
