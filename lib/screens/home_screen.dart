import 'package:fat_heathy/data/bar_chart.dart';
import 'package:fat_heathy/widgets/rtll_line_segments.dart';
import 'package:fat_heathy/widgets/group_bar_chart.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  final double risk5Hip, risk10Hip;
  final double risk5Any, risk10Any;

  HomeScreen({
    Key key,
    this.risk5Hip,
    this.risk10Hip,
    this.risk5Any,
    this.risk10Any,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double _selectedButton = 0;
  double _selectedCategory = 1;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(80.0),
            child: AppBar(
              title: Text('HOME'),
              centerTitle: true,
              bottom: TabBar(
                indicatorWeight: 1.5,
                tabs: [
                  Tab(icon: Icon(Icons.bar_chart_sharp)),
                  Tab(icon: Icon(Icons.stacked_line_chart_sharp)),
                  Tab(icon: Icon(Icons.square_foot_sharp)),
                  Tab(icon: Icon(Icons.circle)),
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 16),
                      Text(
                        'Biểu đồ tiên lượng nguy cơ gãy xương',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Row(
                        children: [
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: RotatedBox(
                                  quarterTurns: -1,
                                  child: Text(
                                    "Nguy cơ gãy xương do loãng xương trong vòng 5 và 10 năm tới.",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 500,
                            width: 320,
                            child: GroupedStackedBarChart(
                              barChart(
                                  widget.risk5Hip.toInt(),
                                  widget.risk10Hip.toInt(),
                                  widget.risk5Any.toInt(),
                                  widget.risk10Any.toInt()),
                              animate: true,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 35, vertical: 30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 30,
                                  width: 30,
                                  color: Colors.red,
                                ),
                                Text('Hip fracture'),
                              ],
                            ),
                            SizedBox(width: 0.0, height: 4),
                            Row(
                              children: [
                                Container(
                                  height: 30,
                                  width: 30,
                                  color: Colors.blue,
                                ),
                                Text('Any osteoporotic fracture'),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                child: Text('Biểu đồ biểu thị tiền lượng loãng xương'),
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: [
                    buildPaddingText(
                        'Nếu không điều trị loãng xương, có khoảng ${widget.risk5Hip.toInt()} trong 100 người giống bạn sẽ gãy xương hông trong 5 năm tới.'),
                    buildPadding(widget.risk5Hip.toInt(), Colors.red[900]),
                    buildPaddingText(
                        'Nếu không điều trị loãng xương, có khoảng ${widget.risk10Hip.toInt()} trong 100 người giống bạn sẽ gãy bất kỳ loại xương nào trong 10 năm tới.'),
                    buildPadding(widget.risk10Hip.toInt(), Colors.red[900]),
                    buildPaddingText(
                        'Nếu không điều trị loãng xương, có khoảng ${widget.risk5Any.toInt()} trong 100 người giống bạn sẽ gãy xương hông trong 5 năm tới.'),
                    buildPadding(widget.risk5Any.toInt(), Colors.blue),
                    buildPaddingText(
                        'Nếu không điều trị loãng xương, có khoảng ${widget.risk10Any.toInt()} trong 100 người giống bạn sẽ gãy bất kỳ loại xương nào trong 10 năm tới.'),
                    buildPadding(widget.risk10Any.toInt(), Colors.blue),
                    SizedBox(width: 0.0, height: 30),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 8.0,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Nguy cơ gãy xương hiện tại',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        SizedBox(width: 0.0, height: 16),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'Nguy cơ 100 người có tình trạng các yếu tố nguy cơ giống bạn sẽ bị gãy xương nếu không điều trị loãng xương.',
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                              ),
                            ),
                            SizedBox(width: 16, height: 0.0),
                            Column(
                              children: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _selectedCategory = 0;
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    primary: Colors.red,
                                    backgroundColor: _selectedCategory == 0
                                        ? Colors.blue
                                        : Colors.white,
                                  ),
                                  child: Text('Any osteoporotic fracture'),
                                ),
                                SizedBox(width: 0, height: 8),
                                TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _selectedCategory = 1;
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    backgroundColor: _selectedCategory == 1
                                        ? Colors.blue
                                        : Colors.white,
                                    primary: Colors.red,
                                  ),
                                  child: Text('Hip fracture'),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(width: 0.0, height: 16),
                        Column(
                          children: <Widget>[
                            LimitedBox(
                              maxHeight: 320,
                              maxWidth: 320,
                              child: Wrap(
                                children: [
                                  for (var i = 0; i < 100; i++)
                                    (i >=
                                            100 -
                                                (_selectedCategory == 1
                                                    ? (_selectedButton == 0
                                                        ? widget.risk5Hip
                                                            .toInt()
                                                        : widget.risk10Hip
                                                            .toInt())
                                                    : (_selectedButton == 0
                                                        ? widget.risk5Any
                                                            .toInt()
                                                        : widget.risk10Any
                                                            .toInt())))
                                        ? Image.asset(
                                            'assets/images/person.png',
                                            height: 32,
                                            width: 32,
                                            fit: BoxFit.cover)
                                        : Image.asset(
                                            'assets/images/person_hide.png',
                                            height: 32,
                                            width: 32,
                                            fit: BoxFit.cover),
                                ],
                              ),
                            ),
                            SizedBox(width: 0.0, height: 8),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _selectedButton = 0;
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    primary: Colors.red,
                                    backgroundColor: _selectedButton == 0
                                        ? Colors.blue
                                        : Colors.white,
                                  ),
                                  child: Text('5 Năm'),
                                ),
                                SizedBox(width: 8, height: 0.0),
                                TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _selectedButton = 1;
                                    });
                                  },
                                  style: TextButton.styleFrom(
                                    backgroundColor: _selectedButton == 1
                                        ? Colors.blue
                                        : Colors.white,
                                    primary: Colors.red,
                                  ),
                                  child: Text('10 Năm'),
                                ),
                              ],
                            ),
                            SizedBox(width: 0.0, height: 16),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${100 - (_selectedCategory == 1 ? (_selectedButton == 0 ? widget.risk5Hip.toInt() : widget.risk10Hip.toInt()) : (_selectedButton == 0 ? widget.risk5Any.toInt() : widget.risk10Any.toInt()))}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 40),
                                    ),
                                    Text(
                                      "Sẽ không \ngãy xương",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 18),
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '${(_selectedCategory == 1 ? (_selectedButton == 0 ? widget.risk5Hip.toInt() : widget.risk10Hip.toInt()) : (_selectedButton == 0 ? widget.risk5Any.toInt() : widget.risk10Any.toInt()))}',
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 40),
                                    ),
                                    Text(
                                      "Sẽ\ngãy xương",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 18),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Padding buildPaddingText(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 35),
      child: Text(text),
    );
  }

  Padding buildPadding(int phanTram, var color) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 20,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '$phanTram %',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          for (var i = 0; i < 100; i++)
            Padding(
              padding: const EdgeInsets.only(left: 1),
              child: Container(
                height: 42,
                width: 2.2,
                color: i < phanTram ? color : Colors.black12,
              ),
            ),
        ],
      ),
    );
  }
}
