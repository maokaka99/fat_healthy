import 'dart:math';
import 'package:fat_heathy/screens/home_screen.dart';
import 'package:fat_heathy/widgets/base_alerdialog.dart';
import 'package:flutter/material.dart';

enum SingingCharacter { lafayette, jefferson }
final TextStyle whiteSubHeadingTextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.white,
  fontWeight: FontWeight.w400,
);
const Color hintTextColor = Color(0xFFE4E0E8);

class InfomationScreen extends StatefulWidget {
  @override
  _InfomationScreenState createState() => _InfomationScreenState();
}

class _InfomationScreenState extends State<InfomationScreen> {
  bool checkedValue = true;
  SingingCharacter _character = SingingCharacter?.lafayette;
  int _valueSex = 0;
  int _valueFall = 0;
  int _valueProir = 0;
  int _valueAge = 50;
  String _strFractures = "1";
  String _strFall = "1";
  int _valueBone = 1;
  int _valueDXA = 1;
  int _value3 = 1;
  double _risk5Hip;
  double _risk10Hip;
  double _risk5Any;
  double _risk10Any;
  List<int> _listAge = [];

  final _valueTscore = TextEditingController();

  void calculateHip(
      double age, double tscore, double fx, double falls, int sex) {
    double y = (age * _valueAge +
        tscore * double.parse(_valueTscore.text) +
        fx *
            double.parse(
                _strFractures.contains("3 or more") ? '3' : _strFractures) +
        falls * double.parse(_strFall.contains("3 or more") ? '3' : _strFall));
    double e = exp(y);
    _risk5Hip = double.parse(
        ((1 - pow(sex == 0 ? 0.999998862 : 0.999896685, e)) * 100)
            .toStringAsFixed(1));
    _risk10Hip = double.parse(
        ((1 - pow(sex == 0 ? 0.999997778 : 0.999793070, e)) * 100)
            .toStringAsFixed(1));
  }

  void calculateAny(double b1, double b2, double b3, double b4, int sex) {
    double fractures =
        double.parse(_strFractures.contains("3 or more") ? '3' : _strFractures);
    double falls =
        double.parse(_strFall.contains("3 or more") ? '3' : _strFall);
    double y = (b1 * _valueAge +
        b2 * double.parse(_valueTscore.text) +
        b3 * fractures +
        b4 * falls);
    double e = exp(y);
    _risk5Any = double.parse(
        ((1 - pow(sex == 0 ? 0.999921076 : 0.995750118, e)) * 100)
            .toStringAsFixed(1));
    _risk10Any = double.parse(
        ((1 - pow(sex == 0 ? 0.999848975 : 0.990905138, e)) * 100)
            .toStringAsFixed(1));
  }

  @override
  void initState() {
    for (var i = 50; i < 96; i++) {
      _listAge.add(i);
    }
    super.initState();
  }

  _confirmTscore() {
    var baseDialog = BaseAlertDialog(
      title: "T-scores là gì?",
      content:
          "T-score là giá trị BMD được chuẩn hóa với giá trị tham chiếu là BMD đỉnh, được xác định bởi BMD của những người trẻ có BMD bình thường có cùng giới tính và sắc tộc.",
      yesOnPressed: null,
      noOnPressed: () {
        Navigator.of(context).pop();
      },
      no: "Đóng",
      yes: "",
    );
    showDialog(context: context, builder: (BuildContext context) => baseDialog);
  }

  @override
  Widget build(BuildContext context) {
    double sizeText = 20.0;
    return Scaffold(
      body: PageView(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildHeader(),
                  SizedBox(width: 0.0, height: 20),
                  buildCardFullName(sizeText),
                  SizedBox(width: 0.0, height: 8),
                  buildCardSex(sizeText, context),
                  SizedBox(width: 0.0, height: 8),
                  buildCardSelected(sizeText, context),
                  SizedBox(width: 0.0, height: 8),
                  buildCardBone(sizeText, context),
                  SizedBox(width: 0.0, height: 8),
                  buildCardScores(sizeText, context, "T-score:",
                      "T-Score between 4 and -5", true),
                  SizedBox(width: 0.0, height: 8),
                  Align(
                      child: Text('OR',
                          style: TextStyle(
                              fontSize: 30, color: Colors.blue[400]))),
                  buildCardDXA(sizeText, context),
                  SizedBox(width: 0.0, height: 8),
                  buildCardBMD(sizeText, context),
                  SizedBox(width: 0.0, height: 8),
                  buildCardDisclaimer(sizeText),
                  SizedBox(width: 0.0, height: 8),
                  buildCheckbox(sizeText),
                  SizedBox(width: 0.0, height: 16),
                  Align(
                    child: ElevatedButton(
                      child: Text(
                        "Calculate Risk Factor".toUpperCase(),
                        style: TextStyle(fontSize: 14),
                      ),
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.red),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Colors.red),
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (_valueSex == 0) {
                          calculateHip(0.107, -1.007, 0.599, 0.212, _valueSex);
                          calculateAny(
                              0.0883, -0.2986, 0.8454, 0.0981, _valueSex);
                        } else if (_valueSex == 1) {
                          calculateHip(
                              0.0507, -0.8417, 0.8127, 0.3614, _valueSex);
                          calculateAny(
                              0.0321, -0.4022, 0.5691, 0.2038, _valueSex);
                        }
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => HomeScreen(
                              risk5Hip: _risk5Hip.roundToDouble(),
                              risk10Hip: _risk10Hip.roundToDouble(),
                              risk5Any: _risk5Any.roundToDouble(),
                              risk10Any: _risk10Any.roundToDouble(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(width: 0.0, height: 32),
                ],
              ),
            ),
          ),
          SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    buildHeader(),
                    SizedBox(width: 0.0, height: 20),
                    buildCardFullName(sizeText),
                    SizedBox(width: 0.0, height: 20),
                    buildSelectAge(sizeText),
                    SizedBox(width: 0.0, height: 20),
                    buildCardScores(
                        sizeText, context, "FN-BND:", "-1 T-score", false),
                    SizedBox(width: 0.0, height: 20),
                    buildCardScores(sizeText, context, "BMD:", "kg/cm²", false),
                    SizedBox(width: 0.0, height: 20),
                    buildCardFall(sizeText, context),
                    SizedBox(width: 0.0, height: 20),
                    buildCardPrior(sizeText, context),
                    SizedBox(width: 0.0, height: 20),
                    buildElevatedButton(),
                    SizedBox(width: 0.0, height: 40),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ElevatedButton buildElevatedButton() {
    return ElevatedButton(
      child: Text(
        "Calculate Risk Factor".toUpperCase(),
        style: TextStyle(fontSize: 14),
      ),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(color: Colors.red),
          ),
        ),
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
        );
      },
    );
  }

  Card buildSelectAge(double sizeText) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildRowAge(sizeText),
          ],
        ),
      ),
    );
  }

  Column buildHeader() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 25),
          child: Align(
            child: Image.asset(
              "assets/images/logo.png",
              height: 80,
            ),
          ),
        ),
        SizedBox(width: 0.0, height: 25),
        Align(
          child: Text(
            'FRACTURE RISK CALCULATOR',
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        Text(
          'Fill out the following to estimate your fracture risk',
          textAlign: TextAlign.justify,
          style: TextStyle(fontSize: 16),
        ),
      ],
    );
  }

  CheckboxListTile buildCheckbox(double sizeText) {
    return CheckboxListTile(
      title: Text(
        "Tôi đã đọc và hiểu sự từ chối trách nhiệm",
        style: TextStyle(fontSize: sizeText, fontStyle: FontStyle.italic),
      ),
      value: checkedValue,
      onChanged: (newValue) {
        setState(() {
          checkedValue = newValue;
        });
      },
      controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
    );
  }

  Card buildCardDisclaimer(double sizeText) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Tuyên bố từ chối trách nhiệm',
              style: TextStyle(fontSize: sizeText, fontWeight: FontWeight.bold),
            ),
            SizedBox(width: 0.0, height: 8),
            Text(
                'Các kết quả được tạo ra bởi ứng dụng của chúng tôi sẽ chỉ đóng vai trò tham khảo. Nếu lo lắng về nguy cơ gãy xương của bạn, bạn nên hỏi ý kiến bác sĩ hoặc chuyên gia xương khớp.',
                style: TextStyle(fontSize: sizeText),
                textAlign: TextAlign.justify),
          ],
        ),
      ),
    );
  }

  Card buildCardBMD(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Thực tế BMD:',
              style: TextStyle(fontSize: sizeText),
            ),
            Stack(
              alignment: Alignment.centerRight,
              children: [
                TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: Icon(Icons.data_usage),
                    hintText: "Đơn vị: g/cm²",
                    hintStyle:
                        whiteSubHeadingTextStyle.copyWith(color: hintTextColor),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Card buildCardDXA(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Máy đo mật độ xương: ',
              style: TextStyle(fontSize: sizeText),
            ),
            Row(
              children: <Widget>[
                Expanded(child: buildListDXA(context, "Bởi DXA GE Lunar", 0)),
                Expanded(child: buildListDXA(context, "Bởi DXA Hologic", 1))
              ],
            )
          ],
        ),
      ),
    );
  }

  ListTile buildListDXA(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text,
          style: TextStyle(
              color: _valueDXA == value ? Colors.blue : Colors.black)),
      leading: Radio(
        value: value,
        groupValue: _valueDXA,
        onChanged: (T) {
          setState(() {
            _valueDXA = T;
          });
        },
      ),
    );
  }

  Card buildCardScores(double sizeText, BuildContext context, String text,
      String hintText, bool isTscore) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              text,
              style: TextStyle(fontSize: sizeText),
            ),
            Stack(
              alignment: Alignment.centerRight,
              children: [
                TextField(
                  controller: _valueTscore,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: hintText,
                    hintStyle:
                        whiteSubHeadingTextStyle.copyWith(color: hintTextColor),
                  ),
                ),
                isTscore
                    ? IconButton(
                        icon: Icon(Icons.my_library_books_sharp,
                            color: const Color(0xfff96800)),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          _confirmTscore();
                        },
                      )
                    : SizedBox.shrink(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Card buildCardBone(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Bạn có thông tin về mật độ xương (BMD) hiện tại của bạn  không?',
              style: TextStyle(fontSize: sizeText),
            ),
            Row(
              children: <Widget>[
                Expanded(child: buildListBone(context, "Có", 0)),
                Expanded(child: buildListBone(context, "Không", 1))
              ],
            )
          ],
        ),
      ),
    );
  }

  Card buildCardSelected(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildRowAge(sizeText),
            SizedBox(width: 0.0, height: 8),
            buildRowFractures(
                sizeText, context, 'Đã từng gãy xương sau 50 tuổi?'),
            SizedBox(width: 0.0, height: 8),
            buildRowFalls(sizeText, context, 'Có té ngã trong 12 tháng qua?'),
            SizedBox(width: 0.0, height: 8),
          ],
        ),
      ),
    );
  }

  Row buildRowFractures(double sizeText, BuildContext context, String text) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(
            text,
            textAlign: TextAlign.start,
            style: TextStyle(fontSize: sizeText),
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.blue.shade200,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 2,
                    blurRadius: 2,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: DropdownButton<String>(
                value: _strFractures,
                underline: SizedBox(),
                items: <String>['0', '1', '2', '3 or more'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value?.toString() ?? "0"),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    _strFractures = newVal;
                  });
                }),
          ),
        ),
      ],
    );
  }

  Row buildRowFalls(double sizeText, BuildContext context, String text) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(
            text,
            textAlign: TextAlign.start,
            style: TextStyle(fontSize: sizeText),
          ),
        ),
        Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.blue.shade200,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 2,
                    blurRadius: 2,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: DropdownButton<String>(
                value: _strFall,
                underline: SizedBox(),
                items: <String>['0', '1', '2', '3 or more'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value?.toString() ?? "0"),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    _strFall = newVal;
                  });
                }),
          ),
        ),
      ],
    );
  }

  Row buildRowAge(double sizeText) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Expanded(child: Text('Tuổi: ', style: TextStyle(fontSize: sizeText))),
        Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.blue.shade200,
          ),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 2,
                    blurRadius: 2,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: DropdownButton<int>(
                hint: Text("Select Age"),
                value: _valueAge,
                underline: SizedBox(),
                items: _listAge.map((int value) {
                  return DropdownMenuItem<int>(
                    value: value,
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(value?.toString() ?? 50),
                      width: 40,
                    ),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    _valueAge = newVal;
                  });
                }),
          ),
        ),
      ],
    );
  }

  Card buildCardSex(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Giới tính: ',
              style: TextStyle(fontSize: sizeText),
            ),
            Row(
              children: <Widget>[
                Expanded(child: buildListTile(context, "Đàn ông", 0)),
                Expanded(child: buildListTile(context, "Phụ nữ", 1))
              ],
            )
          ],
        ),
      ),
    );
  }

  Card buildCardFall(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Fall: ',
              style: TextStyle(fontSize: sizeText),
            ),
            Row(
              children: <Widget>[
                Expanded(child: buildListFall(context, "Yes", 0)),
                Expanded(child: buildListFall(context, "No", 1))
              ],
            )
          ],
        ),
      ),
    );
  }

  Card buildCardPrior(double sizeText, BuildContext context) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Prior fracture: ',
              style: TextStyle(fontSize: sizeText),
            ),
            Row(
              children: <Widget>[
                Expanded(child: buildListPrior(context, "Yes", 0)),
                Expanded(child: buildListPrior(context, "No", 1))
              ],
            )
          ],
        ),
      ),
    );
  }

  Card buildCardFullName(double sizeText) {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Họ và Tên:',
              style: TextStyle(fontSize: sizeText),
            ),
            TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                suffixIcon:
                    Icon(Icons.supervised_user_circle_rounded, size: 30),
                hintText: "Nguyễn Văn A",
                hintStyle:
                    whiteSubHeadingTextStyle.copyWith(color: hintTextColor),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ListTile buildListTile(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text,
          style: TextStyle(
              color: _valueSex == value ? Colors.blue : Colors.black)),
      leading: Radio(
        value: value,
        groupValue: _valueSex,
        onChanged: (T) {
          setState(() {
            _valueSex = T;
          });
        },
      ),
    );
  }

  ListTile buildListFall(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text,
          style: TextStyle(
              color: _valueFall == value ? Colors.blue : Colors.black)),
      leading: Radio(
        value: value,
        groupValue: _valueFall,
        onChanged: (T) {
          setState(() {
            _valueFall = T;
          });
        },
      ),
    );
  }

  ListTile buildListPrior(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text,
          style: TextStyle(
              color: _valueProir == value ? Colors.blue : Colors.black)),
      leading: Radio(
        value: value,
        groupValue: _valueProir,
        onChanged: (T) {
          setState(() {
            _valueProir = T;
          });
        },
      ),
    );
  }

  ListTile buildListBone(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text,
          style: TextStyle(
              color: _valueBone == value ? Colors.blue : Colors.black)),
      leading: Radio(
        value: value,
        groupValue: _valueBone,
        onChanged: (T) {
          setState(() {
            _valueBone = T;
          });
        },
      ),
    );
  }

  ListTile buildListTile2(BuildContext context, String text, int value) {
    return ListTile(
      title: Text(text),
      leading: Radio(
        value: value,
        groupValue: _value3,
        onChanged: (T) {
          setState(() {
            _value3 = T;
          });
        },
      ),
    );
  }
}
