import 'package:fat_heathy/screens/Welcome/welcome_screen.dart';
import 'package:fat_heathy/widgets/body_flash.dart';
import 'package:flutter/material.dart';

class FlashScreen extends StatefulWidget {
  @override
  _FlashScreenState createState() => _FlashScreenState();
}

class _FlashScreenState extends State<FlashScreen> {
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 24.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.white : Color(0xFF7B51D3),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.1, 0.4, 0.7, 0.9],
            colors: [
              Color(0xFF3594DD),
              Color(0xFF4563DB),
              Color(0xFF5036D5),
              Color(0xFF5B16D0),
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 18),
                alignment: Alignment.centerRight,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => WelcomeScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'Skip',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: PageView(
                  physics: ClampingScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (int page) {
                    setState(() {
                      _currentPage = page;
                    });
                  },
                  children: [
                    BodyFlash(
                      urlImage: 'assets/images/onboarding0.png',
                      title: 'Loãng xương là gì?',
                      content:
                          'Bệnh loãng xương, hay còn gọi là bệnh giòn xương hoặc xốp xương.là hiện tượng xương liên tục mỏng dần và mật độ chất trong xương ngày càng thưa dần, điều này khiến xương giòn hơn, dễ tổn thương và dễ bị gãy dù chỉ bị chấn thương nhẹ',
                    ),
                    BodyFlash(
                      urlImage: 'assets/images/onboarding1.png',
                      title:
                          'Ứng dụng tiên lượng nguy cơ gãy xương \ndo loãng xương dành cho ai?',
                      content:
                          'Ứng dụng này hướng đến người già trên 50 tuổi, những người thuộc nhóm có nguy cơ bị loãng xương. ',
                    ),
                    BodyFlash(
                      urlImage: 'assets/images/onboarding2.png',
                      title: 'Cách phần mềm \nhoạt động ',
                      content:
                          'Mô hình toán học mà ứng dụng sử dụng được xây dựng dựa trên số liệu thu thập từ dự án nghiên cứu đoàn hệ về loãng xương lớn nhất nước Úc.',
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildPageIndicator(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
